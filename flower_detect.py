import cv2
import numpy as np
import argparse
import os

dilation_size = 20
dilation_shape = cv2.MORPH_ELLIPSE
erosion_size = 1
erosion_shape = cv2.MORPH_ELLIPSE

lower_hsv = np.array([125, 30, 150])
upper_hsv = np.array([140, 120, 255])

font = cv2.FONT_HERSHEY_SIMPLEX

def load_images_from_folder(folder):
    print('Searching folder', end='', flush=True)
    images = []
    filenames = os.listdir(folder)
    dot_frequency = int(len(filenames) / 10) + 1
    for idx, filename in enumerate(filenames):
        img = cv2.imread(os.path.join(folder, filename))
        if idx % dot_frequency == 0:
            print('.', end='', flush=True)
        if img is not None:
            images.append(img)
    print(' Done')
    return images

def dilatation(mask):
    element = cv2.getStructuringElement(
        dilation_shape, (2 * dilation_size + 1, 2 * dilation_size + 1), (dilation_size, dilation_size))
    dst = cv2.dilate(mask, element)
    return dst

def erosion(mask):
    element = cv2.getStructuringElement(
        erosion_shape, (2 * erosion_size + 1, 2 * erosion_size + 1), (erosion_size, erosion_size))
    dst = cv2.erode(mask, element)
    return dst

def search_contours(mask):
    contours_count = 0
    mask = erosion(mask)
    mask = dilatation(mask)
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        area = cv2.contourArea(contour)
        if 1000 < area < 30000:
            cv2.drawContours(img, [contour], -1, (0, 255, 0), 2)
            contours_count += 1

            M = cv2.moments(contour)
            if M["m00"] != 0:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
            else:
                cX, cY = 0, 0
            cv2.circle(img, (cX, cY), 3, (255, 255, 255), -1)

    return mask, contours_count

def main(folder, show):
    global img
    images = load_images_from_folder(folder)
    if images is np.empty:
        print('Could not open or find the folder: ', folder)
        exit(0)

    flower_count = 0
    images_len = len(images)

    for idx, img in enumerate(images, start=1):
        print('Checking image ', idx ,' of ', images_len)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, lower_hsv, upper_hsv)
        mask, count = search_contours(mask)
        flower_count += count
        if show:
            imgS = cv2.resize(img, (960, 540))
            maskS = cv2.resize(mask, (960, 540))

            cv2.imshow('mask', maskS)
            cv2.imshow('result', imgS)


    print('Flowers found: ', flower_count)

    print('Press any key to quit...')
    cv2.waitKey()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Code for detecting saffron flower in aerial pictures.')
    parser.add_argument('--input', help='Path to input folder.', default='.')
    parser.add_argument('--show', help='Show result images.', default=False)
    args = parser.parse_args()
    main(args.input, args.show)
