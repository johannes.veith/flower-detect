# Flower Detect

Tool for detecting saffron flowers in aerial photos.

## Installation

Install Python and the necessary libraries

## Usage

```bash
python flower_detect.py --input C:\Users\Johannes\Downloads\DJI_0186 --show False
```
For more information:
```bash
python flower_detect.py --help
```
## License
[MIT](https://choosealicense.com/licenses/mit/)